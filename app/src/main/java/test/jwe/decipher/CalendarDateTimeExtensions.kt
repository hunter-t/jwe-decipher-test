package test.jwe.decipher

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*
import kotlin.text.Typography.nbsp

/**
 * Created by Ruslan Arslanov on 05.02.2017.
 */

// ------------------ Calendar creation ----------------- //

fun createCalendarFromMillis(timeMillis: Long): Calendar {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = timeMillis
    return calendar
}

fun createTodaysMidnightCalendar(): Calendar {
    return Calendar
        .getInstance()
        .setHours(0).setMinutes(0).setSeconds(0).setMilliseconds(0)
}

fun createTomorrowsMidnightCalendar(): Calendar = createTodaysMidnightCalendar().addDays(1)

fun createLastDayOfCurrentMonthCalendar(): Calendar {
    return createTodaysMidnightCalendar().apply {
        val lastDayOfMonth = getActualMaximum(Calendar.DAY_OF_MONTH)
        setDayOfMonth(lastDayOfMonth)
    }
}

fun createFirstDayOfCurrentMonthCalendar(): Calendar {
    return createTodaysMidnightCalendar().apply {
        val firstDayOfMonth = getActualMinimum(Calendar.DAY_OF_MONTH)
        setDayOfMonth(firstDayOfMonth)
    }
}

fun createYesterdayCalendar(): Calendar = createTodaysMidnightCalendar().addDays(-1)

fun createCalendarWith(year: Int, month: Int, day: Int): Calendar =
    Calendar.getInstance().setYear(year).setMonth(month).setDay(day)

fun createCalendarWith(month: Int, day: Int): Calendar =
    Calendar.getInstance().setMonth(month).setDay(day)

fun createCalendarWith(day: Int): Calendar =
    Calendar.getInstance().setDay(day)

fun Calendar.getInstance(timeMillis: Long): Calendar {
    val calendar = Calendar.getInstance()
    if (timeMillis != 0L) {
        calendar.timeInMillis = timeInMillis
    }
    return calendar
}


// -------------- Calendar chained setters -------------- //

fun Calendar.setDay(day: Int): Calendar {
    set(DATE, day)
    return this
}

fun Calendar.setDayOfMonth(dayOfMonth: Int): Calendar {
    set(DAY_OF_MONTH, dayOfMonth)
    return this
}

fun Calendar.setMonth(month: Int): Calendar {
    set(MONTH, month)
    return this
}

fun Calendar.setWeekOfMonth(week: Int): Calendar {
    set(WEEK_OF_MONTH, week)
    return this
}

fun Calendar.setYear(year: Int): Calendar {
    set(YEAR, year)
    return this
}

fun Calendar.setHours(hours: Int): Calendar {
    set(HOUR_OF_DAY, hours)
    return this
}

fun Calendar.setMinutes(minutes: Int): Calendar {
    set(Calendar.MINUTE, minutes)
    return this
}

fun Calendar.setSeconds(seconds: Int): Calendar {
    set(Calendar.SECOND, seconds)
    return this
}

fun Calendar.setMilliseconds(millis: Int): Calendar {
    set(Calendar.MILLISECOND, millis)
    return this
}

fun Calendar.addDays(days: Int): Calendar {
    add(Calendar.DATE, days)
    return this
}

fun Calendar.addMonths(months: Int): Calendar {
    add(Calendar.MONTH, months)
    return this
}

fun Calendar.addYears(years: Int): Calendar {
    add(Calendar.YEAR, years)
    return this
}

fun Calendar.subtractMonths(moths: Int): Calendar {
    add(Calendar.MONTH, -moths)
    return this
}

fun Calendar.subtractDays(days: Int): Calendar {
    add(Calendar.DATE, -days)
    return this
}

fun Calendar.addMonth(): Calendar = addMonths(1)


// ------------- Calendar cloning with set -------------- //

fun Calendar.cloneAndSetDay(day: Int): Calendar =
    (clone() as Calendar).setDay(day)

fun Calendar.cloneAndAddMonths(months: Int): Calendar =
    (clone() as Calendar).addMonths(months)

fun Calendar.cloneAndSetWeekOfMonth(week: Int): Calendar =
    (clone() as Calendar).setWeekOfMonth(week)

fun Calendar.cloneAndSubstractMonths(months: Int): Calendar =
    (clone() as Calendar).addMonths(-months)


// ------------ Calendar convenient methods ------------- //

fun Calendar.isTheSameDay(anotherCalendar: Calendar): Boolean =
    this[DATE] == anotherCalendar[DATE]
            && this[MONTH] == anotherCalendar[MONTH]
            && this[YEAR] == anotherCalendar[YEAR]

fun Calendar.isTheSameWeek(anotherCalendar: Calendar): Boolean =
    this[WEEK_OF_MONTH] == anotherCalendar[WEEK_OF_MONTH]
            && this[MONTH] == anotherCalendar[MONTH]
            && this[YEAR] == anotherCalendar[YEAR]

fun Calendar.isToday(): Boolean {
    val todayCalendar = createTodaysMidnightCalendar()
    return this[DATE] == todayCalendar[DATE]
            && this[MONTH] == todayCalendar[MONTH]
            && this[YEAR] == todayCalendar[YEAR]
}

fun Calendar.isTomorrow(): Boolean {
    val tomorrowCalendar = createTodaysMidnightCalendar().addDays(1)
    return this[DATE] == tomorrowCalendar[DATE]
            && this[MONTH] == tomorrowCalendar[MONTH]
            && this[YEAR] == tomorrowCalendar[YEAR]
}

fun Calendar.isYesterday(): Boolean {
    val yesterdayCalendar = createTodaysMidnightCalendar().addDays(-1)
    return this[DATE] == yesterdayCalendar[DATE]
            && this[MONTH] == yesterdayCalendar[MONTH]
            && this[YEAR] == yesterdayCalendar[YEAR]
}

fun Calendar.isTheActualMonth(): Boolean {
    val todayCalendar = createTodaysMidnightCalendar()
    return this[MONTH] == todayCalendar[MONTH]
            && this[YEAR] == todayCalendar[YEAR]
}

fun Calendar.isGreater(anotherCalendar: Calendar): Boolean =
    this.timeInMillis > anotherCalendar.timeInMillis

fun Calendar.isLess(anotherCalendar: Calendar): Boolean =
    this.timeInMillis < anotherCalendar.timeInMillis

fun Calendar.isGreaterOrEqual(anotherCalendar: Calendar): Boolean =
    this.timeInMillis >= anotherCalendar.timeInMillis

fun Calendar.isLessOrEqual(anotherCalendar: Calendar): Boolean =
    this.timeInMillis <= anotherCalendar.timeInMillis

fun Calendar.localizeFirstDayOfWeekInMonth(firstDayOfWeekInLocale: Int): Int {
    val originalFirstDayOfWeek = cloneAndSetDay(1).get(Calendar.DAY_OF_WEEK)
    val isSunday = originalFirstDayOfWeek == Calendar.SUNDAY
    return originalFirstDayOfWeek + when (firstDayOfWeekInLocale) {
        MONDAY -> if (isSunday) 6 else -1
        TUESDAY -> if (isSunday) 5 else -2
        WEDNESDAY -> if (isSunday) 4 else -3
        THURSDAY -> if (isSunday) 3 else -4
        FRIDAY -> if (isSunday) 2 else -5
        SATURDAY -> if (isSunday) 1 else -6
        else -> originalFirstDayOfWeek
    }
}


// --------------------- Date & Time -------------------- //

private val yearFormat = SimpleDateFormat("yyyy", Locale.getDefault())
private val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
private val monthFullNameFormat = SimpleDateFormat("MMMM", Locale.getDefault())
private val dateHumanFormat = SimpleDateFormat("d MMMM yyyy", Locale.getDefault())
private val dateStandardFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

fun parseTimeFromSeconds(timeSeconds: Long): String {
    val millis = timeSeconds * DateUtils.SECOND_IN_MILLIS
    return timeFormat.format(Date(millis))
}

fun parseTimeFromMillis(timeMillis: Long): String = timeFormat.format(Date(timeMillis))

fun parseDateFromMilliss(timeMillis: Long) = dateStandardFormat.format(Date(timeMillis))

fun parseMonthNameInNominative(timeMillis: Long, locale: Locale = Locale.getDefault()): String {
    val defaultMonthName = monthFullNameFormat.format(Date(timeMillis))
    if (locale.language == "ru") {
        val russianNominative = ruMonthCases[defaultMonthName]
        if (russianNominative != null) {
            return russianNominative
        }
    }
    return defaultMonthName
}

private val ruMonthCases = hashMapOf(
    "января" to "Январь",
    "февраля" to "Февраль",
    "марта" to "Март",
    "апреля" to "Апрель",
    "мая" to "Май",
    "июня" to "Июнь",
    "июля" to "Июль",
    "августа" to "Август",
    "сентября" to "Сентябрь",
    "октября" to "Октябрь",
    "ноября" to "Ноябрь",
    "декабря" to "Декабрь"
)

fun parseMonthAndYearInNominative(timeMillis: Long, locale: Locale = Locale.getDefault()): String {
    val date = Date(timeMillis)
    val year = yearFormat.format(date)
    val defaultMonthName = monthFullNameFormat.format(date)
    if (locale.language == "ru") {
        val russianNominative = ruMonthCases[defaultMonthName]
        if (russianNominative != null) {
            return "$russianNominative$nbsp$year"
        }
    }
    return "$defaultMonthName$nbsp$year"
}


// --------------------- System time -------------------- //

val currentTimeSeconds: Long
    get() = secondsToMillis(System.currentTimeMillis())

fun isTimePassed(timeInMillis: Long): Boolean = System.currentTimeMillis() > timeInMillis

fun secondsToMillis(timeInSeconds: Long): Long = timeInSeconds * DateUtils.SECOND_IN_MILLIS

fun millisToSeconds(timeInMillis: Long): Long = timeInMillis / DateUtils.SECOND_IN_MILLIS

