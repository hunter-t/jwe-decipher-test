package test.jwe.decipher

import com.google.gson.annotations.SerializedName

/**
 * Created by Ruslan Arslanov on 09/11/2018.
 */
abstract class BaseJwePayload(

    @SerializedName("iss")
    val tokenIssuer: String,

    @SerializedName("aud")
    val tokenAudience: String,

    @SerializedName("iat")
    val tokenCreationTimeSeconds: Int,

    @SerializedName("exp")
    val tokenExpirationTimeSeconds: Int

)