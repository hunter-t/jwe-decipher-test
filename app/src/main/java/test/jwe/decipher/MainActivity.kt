package test.jwe.decipher

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import com.google.gson.Gson
import com.nimbusds.jose.*
import com.nimbusds.jose.crypto.RSADecrypter
import com.nimbusds.jose.crypto.RSAEncrypter
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jose.crypto.RSASSAVerifier
import kotlinx.android.synthetic.main.activity_main.*
import ru.interfax.sp.data.auth.jwe.payloads.PhoneNumberJwePayload
import java.security.KeyFactory
import java.security.KeyPairGenerator
import java.security.PrivateKey
import java.security.PublicKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // --------------------------------------------------------------- //
        // ----------------- Deserialize test Reg Token ------------------ //
        // --------------------------------------------------------------- //

        val encryptedRegistrationToken =
            "eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkExOTJDQkMtSFMzODQifQ.BRyl5D5JpCPTMMOcUolYC68pH_czJ8UIPhM8mspR68Utlo-fRmZRlHgu2HuQuW5lqkzWALAEOj5AYR098damNYRwl_JnuDuFUUnq7KBcPLw56_Onyr9k3gimRKYNLlWrnhPAKU8MOW-Gnt7vGnXlFU9MljXIE1FYVch6nn3dxsWngrAFN-mbsGowRB2ClnIL5MzNoP8dDc_0pNnbXdPuI_GwVmHeWR7WpKyVRDwvxt7hRFqz8d0L76RN4yO3T6XzWGxaUTJUhN9c8pLMgepWtoXgtPOt4FcZmp239NPG_sY3NH9KJZo6aJ5ScuZY2B-Wtb-XGEyQJ5MGgEVBSMkAl8pa-Mjiaf5Flb-R4U0FDqDnVWhG-CsVFYYewVjOVJ7Lrmwp3Bpk5B79fFbhhfpTe1RAjHYNPAYgWDDXrCpLlY4w4JQKazpq3WrSZTJCnhUXtp7UvP7d9l38Ml1kvdj4qDUeQnM6D2EFxFz76LZ0DQ3bTYk8T76leqCiEH5TwpP53mX3jv30EuNEJ5Yeoo4O34ore-gi-UhF-wKCLOlFG6znOjfz_TSguhgZqp_AxY99SiZfGhN94v3U5588UkIXNW-ZT8VNkEY3PlDekgCK7OATlWMHZno2tGlslpMHTQFLZgCjUD88Z1CXgzNvaDRguWooseUMDQTUv5S_9KYcSmU.uckYlyzbBPLuBpTgYkAzVw.t3TGq6lCzZCZXPJAz48w8THwjDyBQXocotDfUfo6gL9cZDZrJgQ4Ov8N71oQDp6s.Dls9nUqkuEWb5wSRXNArMttBTWys7TeX"

        val decryptedJwe = JWEObject
            .parse(encryptedRegistrationToken)
            .apply { decrypt(RSADecrypter(getTestAppPrivateKey())) }

        val decryptedPayload = decryptedJwe.payload.toString()


        // --------------------------------------------------------------- //
        // ------------- Imitating Client<->Server exchange -------------- //
        // --------------------------------------------------------------- //

        val (privateKey, publicKey) = getAppKeypair()

        val challengePayloadData = buildChallengeRequestPayload()
        val challengePayloadJson = Gson().toJson(challengePayloadData)
        val challengePayload = Payload(challengePayloadJson)

        val jweHeader = JWEHeader(
            JWEAlgorithm.RSA_OAEP_256,
            EncryptionMethod.A256CBC_HS512
        )
        val challengeJweToken = JWEObject(jweHeader, challengePayload)
        val rsaEncrypter = RSAEncrypter(publicKey as RSAPublicKey)
        challengeJweToken.encrypt(rsaEncrypter)
        val compactChallengeJweForm = challengeJweToken.serialize()

        // Deserialize it back.
        val decryptedChallengeJwe = JWEObject
            .parse(compactChallengeJweForm)
            .apply { decrypt(RSADecrypter(privateKey)) }

        val decryptedChallengePayload = decryptedChallengeJwe.payload.toString()


        // --------------------------------------------------------------- //
        // ------------------ Deserialize test Claims -------------------- //
        // --------------------------------------------------------------- //

        val encryptedTestClaimsToken =
            "eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkEyNTZHQ00ifQ.WHaJJMQg5F2r1qKdrHSz5YejYd0ncn_2DkIDprigeg_tAkITR6aT_y3MPeryfU4-d1-c_idSWDaO5IlVIOjuJfIRWXLMzZ-UztIOh9NkedN0L58N-gc_awArRDDjDcUKDAz-tbU0FAeJx2Aq0eAHG2PUJaz5zR--O_u5Mcy2ludRi8RR7Wx1RJ48gaoC270vcv2v6TRSOtrn6j_6C8t5hLrsiK3rG3wzxtvaX2CWc6TdxYJF4KqZp7UEtwPfexC0pHRawa_FHbfP8AR61qxP54AmOXOqnl5OGWPvxA5dkf4bvpksnVURzvlFwuj7KPHyZwX-clyRu52_LdapaVQxTBvd8_6qgxAqJd5xKaZINQH2dvMDcYyIykXrVy5ordiFKPLq-Ny1aqZj6E5FZorUlW8qSe94LKa-UmKndlxMexKww7TKMxJV0OwVFws-jFf4MlDxCNzyogdg1cTuLNDb2F4NPoxroIDqdU8tfInOfWBnO99_JgeLbvTX4S8kfbcOLj4SN_YbjOzr6YOdnUMTek8Dc0l_tMjzfwJ6w_iUW-iLEwsldOLp79-GicomGnJfwIibKuGe0QqaDxJV-RYn8Jtr_Ot6U2RaULiQr75zIdXVfnBpDfuec5VxipEpW_Cutd_bfS4SgewamBL9shrOxeTahLmknFJ2u-jkj9s4UE8.yim1UtRejNtlx-Wa.V57UyjYXVQDX4s8GkOpUvSplQmglFuokrL2JcB2fgbYu9uRxe2S4uHHvXos8ClGeMN9eMIiF8_ceksRONQ6b9120DQ4XKzo4HWP1l4z7zhhoHtGXajB-3FRnmEf1j8PWHSUaKPPZ3hveh6OADGZtPI7WZaf570flDqElbp2g7LWbITDnoVN81iw-OAH4Xg-KkWLFc0tKAGDmLgxmdcCkmqrnZItIEtV9RBJozjxmRsvVVJ7n29FF6Q_f1f-fCgMNtf-TC0prw24AHBrGuNYadgcPHNPX2JfNgw72AuXEDpwoZuVTGOpV1_ujCdNcFjWVzV7xXE5LLLy1bamXL-QMqaC5cSt2Octjmuv-fSv7R5eENbFg7AY6HI4paGrqw-MOqBcopPT4zdzggHQqjml7PAQiTWGUsKxKjfUBRprbmaksckMES7ciM0Y-nU05K2bXcN1pE3UEgetBnlx6hYhwtVxxeQLqCDAC6h-R1dS9XIQqgKckIkzYpHo_hgorIO8LQJF7la1YDlGMLpKZDqtxowSC73IYZa6H58w9aHuCKCNWOQdguK1VsNYZ_DuSbNDfgENMe1uh1nJQ3t_pRxpUc3WldqsBE0bvgOxCRnTTDJvFdqT_OCr-eCIO5HfpcVeOwfBCkpTu00px98fxREFuKEM-YKMfB1gAYCKrqwOIt0ZnmMc7PFzs0KpjplTsjXL9Pz3i-VJkYP47Kek-9FTW9WXzhTxear95gn0JJk9CPmrQoKplQxXbtRQnPMw6DqvR1jshK7_6wAfYHTvH-ry9BETiXKMcsH0n6qfk8bNF-T3ZEegpZ3v8PVEXF9gTjktjGkXhE8_DaC6gYf-ZsPS4EgfiAKD4PEg9GZ8Ohvc4fapyi1CLXDa7F45YnU7strjjzjy8uL_hOxHopy1tLqRYdoHPUsOGxH343JsIwYulZEujHzWxw__erKCJ_MXB0dYXiXI8ZQdYhzuZgrEKJjzZbK2CahmEDEAbaxtyEu3empwSaRBgdtxerJlVxn-NOdROlGfocBJY1Bp4sH94Lm7mp5qLSYd1m-cS6-Df5kL3X4XMcgjYlDKR02f-dKuUOqORe9PFd-hbb3gUmQzQsEDKuZT7oOpkG2Uhqgf861q-ztK9b1lps1kw5dFXvZ6fA6s5OeY4B5m17k-jI4hSiVLwuqEeJPd7i53UGCz1BrAOZ4BuqgLitNQ7N-MCY1RdD1jlYLtjAiUMUmXYDWt2X5Mq5S_sVDWh9FeD9v0ZQ4dG93YkMhGy-sjKwjS0-Bqa4BRbTap984tQxNXe7Jx80zF_1-ieD4NpS-oaUT5AzcLW5Zbp5IBLQC5nOycoVcJm22mqo_WwRru43vSOG2IYeIERSwhBJQo_4Vtcj7jBTT3uZsaP_t4colZ77nlhWT2b8oYxs-Yi3Di5Y-_Raxu6JW4sGY66UUyGtLM2tztKMuLlulOYBTr3sztNiW0Ey6HGonlQqkZXY5PYLAQ2vfWvmj6jWvibnRfp_nTk6JaxsjhgYk7Ikbte2Rt1ysvu_vs8sq5arT50bktZqWtY9ppRpIXUnAOP4CCE17BNMuMdLnqxn79-T6NlwZBEhyvtRViVbYadHoxwI1guhxB3m4TKG7sGvtxsuqkJqznHTNAwsXjZ0ziZnKMj7zi2RUh85CTdhlGP4eCt_vJY9fkdqHsGt4MLdZMBzo9AnzUN95CNkIKLngM_QzEiUqbWq3cESDOfeUW5zJp9Y2vWdXFn-T4o5EQwizkSC7Jm10ejHb1Q1Lhq1NzQ0UwsFVt6PR56UcFBsKiQpbrgyZxSxjXNqSJavDgPdkT0QGgI9qP7UaVF1xdr20L6kEPGQnzXavpbL3-qDe4fKuKhpZPK3xMNb9dVounI8YS6HHRYyjRtDIOkCh1BKjdDSiulJsGE29UZMzdjKwGTitaDwe6pInSwfbmJ-fpugqyfLfbwOiGO4ZCnWGQNe8qBB_pWAz3PVhBhub78JQ0D3JPvQexBsle52P0sBzd1Jh8VEIb9Drk97CEmuT1OKkH7NpSqn10we4Z3VOGYwgZL6WcPAYEiYwOV8bf6BspZBHhd0H7xdyhB4aJ-a3e0fZkRlJT4p65TQhXVITeMwYcLM_Q4APqPWYdNx7JDE3RkPuucxPrFD7dSwJjR2Ijbs_iefnJP5F1rGsf0XybqBD9khK0k62HaBDfgi76eWQBbbU8VTYk9HXh0wWqAlRWIH1SD-MVsF8NL0APP9_aivKNmZicCU75YtPncHzvr2-ncYKiAq7kV6t7UgTLqL5gplPMBc3RxJwlkiRNhbhG-dv3j7qmPuRA_rHSnxwAFIoByqIJw4zPq3_im80m2ugIaEtd6oLpZBBdEjpS-7zS2ZcNyc9qfiDMwmoAL7CmBaXNweY_Y-wSowCCKbe19Xgr-4vEcM74KUMP05-SvZU2FThtkL_jkD1tVXfF7u3DNhYzbWlIBrhFrCty2OTFv-GZBqfcWMz3LlZFbRFx_LDuW1mFDY7zjB--3N3khGaXpFkxyrWkGti3e_v89K3BdCDCoii-yPI27zPgTTKo5v9s9tPgckRY9FtS9XVnrsRJenc-N-QCQVqV8p7jzoN3QXpFxueYkVjlXgp8w7XWNVJzZXBkZsidfPNiVB4iduVVE90j-LFozv9XwdBW9Z6xiGH8LkgsfFAj3gkwnXgD29FL3qU_EsdFCfF2G2f2iOzl6lj6qmHcDWrzpQNf45mLaC4kcvoUBeF6K0jRVfwR6C2n6RbXKZqOyKWLzC5r5XFo9OJtnC5yODqyPgzJV478tOE54fNUbelU7GaJteAh322SiSzPBtkhaxGJ-2u5A9pW0NScZulvSzbVf_I-eMxUmuLFlbKOXfZ12MTmC_eTzO3tzwrpqOO_uJD5kqadXLVLQyux96cggqoanvF5L4_l-V8SUVXNfDusGqc2KuiOnB8vndCoAUok2VCr9jsheuO7UPSsxt_Gq3WTgN0ai0vN2gJuVspqJuJXcvHFFJ-gTKeLSG3wdt80C5ysvMNq5iaWA-3DgRcXUc2xU-g3U-C1E3tUDYgwHmidO0ppuV9qSpxgJmTopaxbjvkGOYtPh2iKJBntdvdztrhuyxMLEhWFSCepw3LQcUdZsw0lAbpNV09ygDtdEH2WPK7fZBYgUYD35LkaDw9Ng5xR2eMXxf3pieSXUvQ.sO1UWp-RTwjiNdDMEXeM0g"

        val decryptedTestClaimsJwe = JWEObject
            .parse(encryptedTestClaimsToken)
            .apply { decrypt(RSADecrypter(getTestAppPrivateKey())) }

        val decryptedTestClaimsPayload = decryptedTestClaimsJwe.payload.toString()


        // --------------------------------------------------------------- //
        // -------- Serialize and deserialize X-S-Signature in JWS ------- //
        // --------------------------------------------------------------- //

        val sxSignture =
            "ccee6744-d617-4b13-abf3-5977cd13879c026c4631-3adc-4e85-902c-e651365dc4efhttp://mobile-api.spark-interfax.ru/api/v0.1/auth/tokenGrantType=AuthorizationCode&RegCode=iddqd"

        val sxSigntureJws = JWSObject(
            JWSHeader(JWSAlgorithm.RS512),
            Payload(sxSignture)
        ).apply {
            sign(RSASSASigner(privateKey))
        }

        val seriazliedJws = sxSigntureJws.serialize()

        // Verity it and deserialize.
        val jwsVerifier = RSASSAVerifier(publicKey)
        val deserializedSxJws = JWSObject.parse(seriazliedJws)

        val deserializedAndVerifiedSxSignature = if (deserializedSxJws.verify(jwsVerifier)) {
            getString(R.string.decrypted_s_x_signature, deserializedSxJws.payload.toString())
        } else {
            getString(R.string.error_s_x_signature_decryption)
        }


        // --------------------------------------------------------------- //
        // ---------------- Setting all data to TextViews ---------------- //
        // --------------------------------------------------------------- //

        decodedRegTokenTextView.text = getString(R.string.decrypted_reg_token, decryptedPayload)
        encodedAndDecodedTokenTextView.text = getString(
            R.string.encrypted_and_decrypted_request,
            decryptedChallengePayload
        )
        encodedAndDecodedSxSignatureTextView.text = deserializedAndVerifiedSxSignature
        decodedClaimsTokenTextView.text = getString(
            R.string.decrypted_claims,
            decryptedTestClaimsPayload
        )
    }

    private fun getAppKeypair(): Pair<PrivateKey, PublicKey> {
        return KeyPairGenerator
            .getInstance("RSA")
            .apply {
                initialize(4096)
            }
            .generateKeyPair()
            .let { pair ->
                Pair(pair.private, pair.public)
            }

        // Uncomment to use test keys.
        // return Pair(
        //     getTestAppPrivateKey(),
        //     getTestAppPublicKey()
        // )
    }

    private fun getTestAppPrivateKey(): PrivateKey {
        var encodedRsaPrivateKey = "-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIIJKQIBAAKCAgEAwGSF1Bmu/nurmnz+3Y0kfiRcnysFcEzJE0q0qpv078/rF1n1\n" +
                "pv9MJF3psHdj84O4ibPn66/FR6Qy2IOw1AgWaed4olnlTP8MGpYD1lth0Wm/Tr4M\n" +
                "BzE1Lr4GgWCt6KK5FiGA/n49eOd/bz2T80S9M5DArWVxQLD5RfhRqIMvpwKq4ZTs\n" +
                "o5YsyJLREqAFW8VLXIMA7J8lOjUy/+yr4AWz6LnI3JQHh763mfL24n5uxHDxJXHJ\n" +
                "88BX3WeHa19r7YjwTnbPhTcXZuyjZEHsTlHXr1dbpuq4lm3cOOUI1Pc/r3LnyLUr\n" +
                "PRtjc9A+vTDqKFCFA577CCmym7+TENk0EGEyrZFDJ77zucjLnXayvv3eagM3+7fS\n" +
                "FHTOtXrgEE1QJlQeiiZ2K/PKHI8pp8o0pd3bOxnsTTbEeJOMkW36oU2kigUrFMUb\n" +
                "7i60BiPJ2KhTXCsvl3kNyQ4t9tLMSM9y8zgJ+uu1m3uU2KvLRyYjhwkrN1sNTrDq\n" +
                "bRTwFNYzrFlbn60ds5ei0EBgyRyFcQ13c+CCKRsE8k6EOhdXJ48mp4Y6mqHDnuTa\n" +
                "o47gxQUpM5sn7deRDw77t8PB0zVaTP9JB9orE9rzM+rA6Q7yN2qquoekxkk9s4WG\n" +
                "xQukqB5M+y1H9vUlWrFUssAKR6qPwLkghC/Ays81Zb86rs19omUnEcfYN6sCAwEA\n" +
                "AQKCAgAzQZS18THB8sjBG9P6yoOSZ7WVYizMwmg/j4OagHo8+bO0QO9FUnbCqZJ8\n" +
                "xRIH6mvbo+aB0A5LC9/P9yXBF3aBOH3XIwQIA1sCKj1Ten0RAY9KNy+VKvL0RQQO\n" +
                "q+cpN1EAKGTpaFCQSXyV8l3cTc/qUQJEL+aASaXfkkyPDIRBhmBoQPjajEPaG+M/\n" +
                "hwpmbKWrO+ufHrcYTtwLes4/nMVVl0mAGp/29kSL60MDDJQOGdjlFO+s1O2TuuVF\n" +
                "IeLrIbSD8vtOjmLZtOnC6B1AsGU5/ZgZXCdZTGODdOFpe4WoBIDj5ABlA0r3q99L\n" +
                "ICECJJGLaWuPvSwsell//53zTzHkvW4n1I6cQYurUVUtsdekTII6rLH41NiYtPxQ\n" +
                "4fiPQme7FDOzAKlF+p6hOSEiI4AHOogt2shZNrygAk9nmgPSc4JeywqaCH272jp6\n" +
                "nvd5GvKCHeXPvMXtJCN4aGes3fIZWrgrMaa870Bbl/wWaqutJL3f8aGIRsrclpgI\n" +
                "7BGttPTlwvYCfWLuX0+zyKbEY1tWEWDchEJwNYrPm7uMdgjj1/Baj62Hi9hazHYj\n" +
                "49Hmh4TXDb4yjAyVC3ubG1grCGpmFN07rYb2EWbuE0AjKTw/Yz0VpOnN+JoWHH6H\n" +
                "lWzUy6+vToW74pacURBiP/p5zhDWKhfesvpqEfdjbERBKrabAQKCAQEA7U2iw+Rx\n" +
                "5LMrV48vY9+q5aTSUvgDZpKHAwX2r9m1EPO4go6IXY/jy6QnvE5Ne/sc2x8pFJPm\n" +
                "g9B/3Xdlc81zZE/bRoHvvuN5srQbBt7ws4igY74kdmvVZy4N7J8aRN/EiVvRkZkf\n" +
                "PtAsUkWQIIXZggnew7p3+cLnt+Du6EjkUb9FEh9Es52lXMGKugLb0k5t+SGlBEAU\n" +
                "AG6LrYXmQShOk+hIhDHRzjFbZFnNmdqip90fzET1eM0/vAryzSXgZRA+S+P4VZsJ\n" +
                "JBPiMH+mTV9Fp7SJfurbWEulGkGK8K0GRNkp0Byza/G5ixzmS88zRTgyUmKVgYrt\n" +
                "MwwWVANAbJuCiwKCAQEAz40MXCVDFF06RhL6TBcr5+7mUObjYsj5MTlvU09dVk0q\n" +
                "QkX1C0kUZ0I7nX65b6BEzGMZaoeLzKwxfBySOBi1hDlSSDbNxZ7eULxKIYMUsOYO\n" +
                "IEeGhubf/0kfjsiCentVVqhKccjWmnkZLjCunROCyj+ZsP4UEVYQLvheuR0lLbPI\n" +
                "qoY4v0vaOtZyI3GWcNpFeSwdiK7wx0dPIc01Ir4M77Vp9zVaJgfuznvCwg2CLXvF\n" +
                "OwM7bWUMK9Ny9fmJoE7CkUxWHB4jl6HBwNnf1aO9wAaonkge6wEO4jH4ICea4Oj5\n" +
                "A6FLZAL9Fxw9XIiYZok8xvvbDJThRI+dBXk/VlBjYQKCAQAShVutxFerTNyWIePF\n" +
                "/L/+2mVb+mVxjQAimUWOpgQ+xXbiIbO88a0KyM4dIVNk2M2AwKbZJ033fb9tDpOJ\n" +
                "fUCDVMQ9dYi7sk9eRjZRy/anXIOAJWf9Ih9uxSZrEGPhekGBovvxTC0nHuJGrY05\n" +
                "LJk0UAbl6djRUof5TwH2eTHwu2ftqSkYh7ZPTf/A0tZLTLnClwClG0c0XWxjZ673\n" +
                "6OhxHNEEGEtSt4F1jJjCaFEHnveLXtp1yjcuOwCGSG2CFcl27N0F+Wq2OSMPO/dD\n" +
                "1KrAExQkJrpl4smJChKJBEgX+YcM6iCKzTv5ApFq20oaHe+3hGA80xX+RPQ9LF/L\n" +
                "2jtPAoIBAQCNzjf4qTjYrA3JnOPIozhLYi2YbvhDIT8pA63tRNdnxJcv6q5PBKJd\n" +
                "HqKuq8u02dLHtpnB8FRzBd9VhgvdKfyzF5sBXfhfonkQvmUD2pSDfNt/8H6Yoxu+\n" +
                "R065doO3fZbD+33JmkgOCNlMdtSG27LLzoKKrD43zg6GmQnW/nPNcv2SjNwuz3uO\n" +
                "IcCgONZpyqgW7y5KmgRGk5EcbiF02FbsXI3G6nvqahcemu5pOWONAfiypjWvO5AC\n" +
                "TZlJODJxBf7n2TkoQutciZdg1TuasyJNFf+VDX+aXwG1SCDYj1bBfkvM0CI7ddNI\n" +
                "O/Isy+Mcg320EpYKVZpIBrto5vuAVLJhAoIBAQCYAUDaJBacqiq0FKIv4YLoODnl\n" +
                "HyfSw03cAGCNlY0inTpVlLDR6qaK8wJgeiQDRiUhhkP4+CEtrzSVBxEIzWoprjvw\n" +
                "LYsXxC1ktH9sBpuWQJ0eRx4sBbyymthkLRfpy7mFXiGXLlD7j30Dw2kX4MNquu2A\n" +
                "9DDpKVQIYmnt8gPCSaRUxKgVWy7nO/AiPRppuj/RfnYIlcNx0KHik3EhO4E2QvPI\n" +
                "Fx5p09yZSl4qr+afgMtEeQfnTwOnpAxOZZrCtJVaVxhSXzJbN/Q/JgWojqnpLdlo\n" +
                "ZP58DoL305JcVywo0UakLYPY37gtXgGtlLtVcdp6tC7NICbD6KEMzuwXzsSh\n" +
                "-----END RSA PRIVATE KEY-----\n"

        encodedRsaPrivateKey = encodedRsaPrivateKey
            .replace("-----BEGIN RSA PRIVATE KEY-----", "")
            .replace("-----END RSA PRIVATE KEY-----", "")
            .replace("\n", "")

        return generatePrivate(encodedRsaPrivateKey)
    }

    private fun generatePrivate(endocdedRsaPrivateKey: String): PrivateKey {
        return KeyFactory
            .getInstance("RSA")
            .generatePrivate(getPrivateSpecs(endocdedRsaPrivateKey))
    }

    private fun getPrivateSpecs(encodedString: String): PKCS8EncodedKeySpec {
        val keyBytes = Base64.decode(encodedString, Base64.DEFAULT)
        return PKCS8EncodedKeySpec(keyBytes)
    }

    private fun getTestAppPublicKey(): PublicKey {
        var encodedRsaPublicKey = "-----BEGIN PUBLIC KEY-----\n" +
                "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAwGSF1Bmu/nurmnz+3Y0k\n" +
                "fiRcnysFcEzJE0q0qpv078/rF1n1pv9MJF3psHdj84O4ibPn66/FR6Qy2IOw1AgW\n" +
                "aed4olnlTP8MGpYD1lth0Wm/Tr4MBzE1Lr4GgWCt6KK5FiGA/n49eOd/bz2T80S9\n" +
                "M5DArWVxQLD5RfhRqIMvpwKq4ZTso5YsyJLREqAFW8VLXIMA7J8lOjUy/+yr4AWz\n" +
                "6LnI3JQHh763mfL24n5uxHDxJXHJ88BX3WeHa19r7YjwTnbPhTcXZuyjZEHsTlHX\n" +
                "r1dbpuq4lm3cOOUI1Pc/r3LnyLUrPRtjc9A+vTDqKFCFA577CCmym7+TENk0EGEy\n" +
                "rZFDJ77zucjLnXayvv3eagM3+7fSFHTOtXrgEE1QJlQeiiZ2K/PKHI8pp8o0pd3b\n" +
                "OxnsTTbEeJOMkW36oU2kigUrFMUb7i60BiPJ2KhTXCsvl3kNyQ4t9tLMSM9y8zgJ\n" +
                "+uu1m3uU2KvLRyYjhwkrN1sNTrDqbRTwFNYzrFlbn60ds5ei0EBgyRyFcQ13c+CC\n" +
                "KRsE8k6EOhdXJ48mp4Y6mqHDnuTao47gxQUpM5sn7deRDw77t8PB0zVaTP9JB9or\n" +
                "E9rzM+rA6Q7yN2qquoekxkk9s4WGxQukqB5M+y1H9vUlWrFUssAKR6qPwLkghC/A\n" +
                "ys81Zb86rs19omUnEcfYN6sCAwEAAQ==\n" +
                "-----END PUBLIC KEY-----\n"

        encodedRsaPublicKey = encodedRsaPublicKey
            .replace("-----BEGIN PUBLIC KEY-----", "")
            .replace("-----END PUBLIC KEY-----", "")
            .replace("\n", "")

        return generatePublic(encodedRsaPublicKey)
    }

    private fun generatePublic(encodedRsaPublicKey: String): PublicKey {
        return KeyFactory
            .getInstance("RSA")
            .generatePublic(getPublicSpecs(encodedRsaPublicKey))
    }

    private fun getPublicSpecs(encodedString: String): X509EncodedKeySpec {
        val keyBytes = Base64.decode(encodedString, Base64.DEFAULT)
        return X509EncodedKeySpec(keyBytes)
    }

    private fun buildChallengeRequestPayload(): PhoneNumberJwePayload {
        val currentTime = millisToSeconds(createTodaysMidnightCalendar().timeInMillis).toInt()
        val expTime = millisToSeconds(createTomorrowsMidnightCalendar().timeInMillis).toInt()

        return PhoneNumberJwePayload(
            apiToken = "026c4631-3adc-4e85-902c-e651365dc4ef",
            deviceId = "ccee6744-d617-4b13-abf3-5977cd13879c",
            tokenCreationTimeSeconds = currentTime,
            tokenExpirationTimeSeconds = expTime,
            tokenIssuer = "ru.interfax.spark",
            tokenAudience = "https://mobile-api.spark-interfax.ru"
        )
    }

}
