package ru.interfax.sp.data.auth.jwe.payloads

import com.google.gson.annotations.SerializedName
import test.jwe.decipher.BaseJwePayload

/**
 * Created by Ruslan Arslanov on 26/10/2018.
 */
class PhoneNumberJwePayload(

    @SerializedName("t_api")
    val apiToken: String,

    @SerializedName("sub")
    val deviceId: String,

    tokenIssuer: String,
    tokenAudience: String,
    tokenCreationTimeSeconds: Int,
    tokenExpirationTimeSeconds: Int

) : BaseJwePayload(
    tokenIssuer,
    tokenAudience,
    tokenCreationTimeSeconds,
    tokenExpirationTimeSeconds
)